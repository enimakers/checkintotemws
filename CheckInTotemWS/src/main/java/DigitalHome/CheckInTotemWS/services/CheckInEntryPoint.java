package DigitalHome.CheckInTotemWS.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.inject.Singleton;

import DigitalHome.CheckInCommon.databaseClient.CassandraClient;
import DigitalHome.CheckInCommon.restClient.FaceReconRestClient;
import DigitalHome.CheckInCommon.transport.ResponseRegisterUser;
import DigitalHome.CheckInCommon.transport.UserData;

@Path("/CheckInEntryPoint")
@Singleton
public class CheckInEntryPoint{
	
    @POST
    @Path("registerUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerUser(UserData userData) {
    	
    	// Set response message to OK, it will be changed in case of error.
    	String outputMessage = "ok";
    	
    	// Temporary block for debug
    	System.out.println("----------------------------");
    	System.out.println("Registering new user");
    	System.out.println("----------------------------");
    	System.out.println("Name = " + userData.getName());
    	System.out.println("Temperature = " + userData.getPref_temperature().toString());
    	System.out.println("Color = " + userData.getPref_color());
    	System.out.println("Music = " + userData.getPref_music());
    	System.out.println("Number of images = " + userData.getImages_b64().size());
    	System.out.println("----------------------------");
    	
    	try {
        	// Save images to gallery
        	FaceReconRestClient restClient = new FaceReconRestClient();
			restClient.addImagesToGallery(userData.getId(), userData.getImages_b64());
			
	    	// Save profile to Cassandra DB
	    	//----------------------------------
	    	
	    	// Create Cassandra client and connect
			
	    	CassandraClient client = new CassandraClient();
	    	client.connect();
	
	    	// Save data to cassandra
	    	try {
				client.saveUserData(userData);
			} catch (Exception e) {
				throw e;
			} finally {
				client.close();
			}
	    	
		} catch (Exception e) {
			e.printStackTrace();
			outputMessage = e.getMessage();
		}
    	
    	// Return result to caller
    	ResponseRegisterUser output = new ResponseRegisterUser();
    	output.setMessage(outputMessage);    	
    	
    	if(outputMessage.equals("ok")){
    		return Response.ok(output).build();
    	} else {
    		return Response.serverError().entity(outputMessage).build();
    	}
    		
    }
}