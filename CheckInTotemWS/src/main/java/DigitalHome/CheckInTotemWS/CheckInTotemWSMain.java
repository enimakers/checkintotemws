package DigitalHome.CheckInTotemWS;

import com.google.inject.Guice;
import com.google.inject.Stage;
import com.google.inject.servlet.GuiceFilter;

import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInTotemWS.modules.CheckInServletModule;

import java.util.EnumSet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class CheckInTotemWSMain {
	
	// Configuration file name for this class
	private static final String configurationFileName  = "CheckInTotemWSMain";	
	
	public static void main(String[] args) throws Exception {
		
		// Get configuration
		// ----------------------------------------
		Configuration config = new Configuration(configurationFileName);
		
		int serverPort = Integer.parseInt(config.getProperties().getProperty("serverPort"));		
		
		// Start server
		// ----------------------------------------
		Guice.createInjector(
               Stage.PRODUCTION,
               new CheckInServletModule()
           );
       
		Server jettyServer = new Server(serverPort);

		// Create the context
		ServletContextHandler context = new ServletContextHandler(jettyServer, "/", ServletContextHandler.SESSIONS);

		// Create Guice filters
		context.addFilter(GuiceFilter.class, "/*", EnumSet.<javax.servlet.DispatcherType>of(javax.servlet.DispatcherType.REQUEST, javax.servlet.DispatcherType.ASYNC));
		context.addServlet(DefaultServlet.class, "/*");

		try {
			jettyServer.start();
			jettyServer.join();
		} finally {
			jettyServer.destroy();
		}
   }
}